package pl.bb166.inzchat.auth.domain.dto;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GroupDTO {
    @Getter @Setter
    private String name;
}
