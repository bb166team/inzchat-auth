package pl.bb166.inzchat.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.bb166.inzchat.auth.domain.dto.UserDTO;
import pl.bb166.inzchat.auth.service.UserService;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("user")
public class UserController {

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("{nickname}")
    public ResponseEntity<Void> addUserToUser(Principal principal, @PathVariable String nickname) {
        return this.userService.addUserToUser(principal.getName(), nickname) ?
                ResponseEntity.status(HttpStatus.CREATED).build() :
                ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @DeleteMapping("{username}")
    public ResponseEntity<Void> removeUserFromUser(Principal principal, @PathVariable String username) {
        return this.userService.removeUserFormUser(principal.getName(), username) ?
                ResponseEntity.status(HttpStatus.CREATED).build() :
                ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping
    public List<UserDTO> getAllUsers(Principal principal) {
        return this.userService.getAllUserForUser(principal.getName());
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("{query}")
    public List<UserDTO> search(@PathVariable String query, Principal principal) {
        return userService.search(query, principal.getName());
    }
}
