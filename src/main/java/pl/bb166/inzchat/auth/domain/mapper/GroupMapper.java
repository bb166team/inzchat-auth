package pl.bb166.inzchat.auth.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import pl.bb166.inzchat.auth.domain.dto.GroupDTO;
import pl.bb166.inzchat.auth.domain.entity.Group;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GroupMapper {
    GroupDTO roomToRoomDTO(Group group);
}