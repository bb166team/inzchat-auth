FROM java:8-jre-alpine

COPY target/auth-1.0-SNAPSHOT.jar /opt/app.jar


ENTRYPOINT ["java", "-jar", "/opt/app.jar"]