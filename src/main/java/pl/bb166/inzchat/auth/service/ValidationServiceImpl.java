package pl.bb166.inzchat.auth.service;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bb166.inzchat.auth.domain.ValidationError;
import pl.bb166.inzchat.auth.domain.dto.RegisterUserDTO;
import pl.bb166.inzchat.auth.repository.UserRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class ValidationServiceImpl implements ValidationService {

    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Set<ValidationError> validateRegisterRequest(RegisterUserDTO registerUserDTO) {
        Set<ValidationError> errors = new HashSet<>();

        checkUserAvailability(registerUserDTO)
                .ifPresent(errors::add);

        validateEmailAddress(registerUserDTO)
                .ifPresent(errors::add);

        checkEmailAvailability(registerUserDTO)
                .ifPresent(errors::add);

        checkUsernameStructure(registerUserDTO)
                .ifPresent(errors::add);

        checkPasswordStructure(registerUserDTO)
                .ifPresent(errors::add);

        return errors;
    }

    private Optional<ValidationError> checkUserAvailability(RegisterUserDTO registerUserDTO) {
        return userRepository
                .getUserByUsername(registerUserDTO.getUsername())
                .map(user -> ValidationError.USER_AVAILABLE);
    }

    private Optional<ValidationError> validateEmailAddress(RegisterUserDTO registerUserDTO) {
        return Optional.of(
                !EmailValidator.getInstance().isValid(registerUserDTO.getEmail())
        ).flatMap(v -> v ? Optional.of(ValidationError.EMAIL_STRUCTURE) : Optional.empty());
    }

    private Optional<ValidationError> checkEmailAvailability(RegisterUserDTO registerUserDTO) {
        return userRepository
                .getUserByEmail(registerUserDTO.getEmail())
                .map(user -> ValidationError.EMAIL_AVAILABLE);
    }

    private Optional<ValidationError> checkUsernameStructure(RegisterUserDTO registerUserDTO) {
        return Optional.of(ValidationError.USERNAME_STRUCTURE)
                .flatMap(error ->
                            registerUserDTO.getUsername().length() < 5 ||
                                    registerUserDTO.getUsername().length() > 32
                                ? Optional.of(error) : Optional.empty()
                        );
    }

    private Optional<ValidationError> checkPasswordStructure(RegisterUserDTO registerUserDTO) {
        return Optional.of(ValidationError.PASSWORD_STRUCTURE)
                .flatMap(error ->
                            registerUserDTO.getPassword().length() < 5 ||
                                    registerUserDTO.getUsername().length() > 32 ?
                                    Optional.of(error) : Optional.empty()
                        );
    }
}
