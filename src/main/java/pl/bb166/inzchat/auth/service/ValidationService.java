package pl.bb166.inzchat.auth.service;

import pl.bb166.inzchat.auth.domain.ValidationError;
import pl.bb166.inzchat.auth.domain.dto.RegisterUserDTO;

import java.util.Set;

public interface ValidationService {
    Set<ValidationError> validateRegisterRequest(RegisterUserDTO registerUserDTO);
}
