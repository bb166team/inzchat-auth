package pl.bb166.inzchat.auth.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bb166.inzchat.auth.domain.ValidationError;
import pl.bb166.inzchat.auth.domain.dto.RegisterUserDTO;
import pl.bb166.inzchat.auth.domain.dto.RegisterUserValidationDTO;
import pl.bb166.inzchat.auth.domain.entity.Role;
import pl.bb166.inzchat.auth.domain.entity.User;
import pl.bb166.inzchat.auth.domain.mapper.UserMapper;
import pl.bb166.inzchat.auth.repository.UserRepository;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class RegisterServiceImpl implements RegisterService {
    private static final Logger logger = LoggerFactory.getLogger(RegisterServiceImpl.class);

    private UserRepository userRepository;

    private SendEmailService sendEmailService;

    private ValidationService validationService;

    private UserMapper mapper;

    private MessageDigest messageDigest;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setSendEmailService(SendEmailService sendEmailService) {
        this.sendEmailService = sendEmailService;
    }

    @Autowired
    public void setValidationService(ValidationService validationService) {
        this.validationService = validationService;
    }

    @Autowired
    public void setMapper(UserMapper mapper) {
        this.mapper = mapper;
    }

    @Autowired
    public void setMessageDigest(MessageDigest messageDigest) {
        this.messageDigest = messageDigest;
    }

    @Override
    public Optional<RegisterUserValidationDTO> registerNewUser(RegisterUserDTO registerUserDTO) {
        Set<ValidationError> errors = validationService.validateRegisterRequest(registerUserDTO);

        if (errors.isEmpty()) {
            String sha = generateUniqueSHA256String();
            User user = mapper.registerUserDTOToUser(registerUserDTO);
            user.setActivationHash(sha);
            user.setEnabled(true);

            Role role = Role.builder().role("ROLE_USER").user(user).build();
            user.setRoles(Collections.singleton(role));

            //sendEmailService.sendActivationMessage(registerUserDTO.getEmail(), sha);

            userRepository.save(user);
            return Optional.empty();
        } else {
            return Optional.of(new RegisterUserValidationDTO(errors));
        }
    }

    private String generateUniqueSHA256String() {
        String sha;
        do {
            sha = bytesToHex(
                    messageDigest.digest(
                            Long.toString(ThreadLocalRandom
                                    .current()
                                    .nextLong()
                            ).getBytes(StandardCharsets.UTF_8)
                    )
            );
        } while (userRepository.getUserByActivationHash(sha).isPresent());

        return sha;
    }

    private String bytesToHex(byte[] sha) {
        BigInteger no = new BigInteger(1, sha);
        StringBuilder stringBuilder = new StringBuilder(no.toString(16));

        while (stringBuilder.length() < 32) {
            stringBuilder.insert(0, "0");
        }
        return stringBuilder.toString();
    }

    @Override
    public void activateUser(String sha) {
        Optional<User> user = userRepository.getUserByActivationHash(sha);

        user.ifPresent(usr-> {
            if (sha.equals(usr.getActivationHash())) {
                usr.setEnabled(true);
                usr.setActivationHash(null);
                userRepository.save(usr);
            }
        });
    }
}