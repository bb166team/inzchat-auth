package pl.bb166.inzchat.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.bb166.inzchat.auth.domain.entity.User;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> getUserByUsername(String username);
    Optional<User> getUserByEmail(String email);
    Optional<User> getUserByActivationHash(String activationHash);

    @Query(value = "select u1.* from user_ u1 left join user_user uu on uu.second_id = u1.id " +
                    "left join user_ u2 on u2.id = uu.first_id " +
                    "where (u1.username like %:usernameToSearch% and u1.username != :loggedUsername and u2.username is null) " +
                    "or (u1.username like %:usernameToSearch% and u1.username != :loggedUsername and u2.username != :loggedUsername)",
            nativeQuery = true)
    List<User> searchByUsername(String usernameToSearch,
                    String loggedUsername);
}