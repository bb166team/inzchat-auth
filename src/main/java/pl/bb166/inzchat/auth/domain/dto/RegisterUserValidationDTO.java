package pl.bb166.inzchat.auth.domain.dto;

import lombok.*;
import pl.bb166.inzchat.auth.domain.ValidationError;

import java.util.Set;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RegisterUserValidationDTO {
    @Getter
    private Set<ValidationError> errors;
}
