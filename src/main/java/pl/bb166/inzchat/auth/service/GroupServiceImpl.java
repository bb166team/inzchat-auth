package pl.bb166.inzchat.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bb166.inzchat.auth.domain.dto.GroupDTO;
import pl.bb166.inzchat.auth.domain.entity.Group;
import pl.bb166.inzchat.auth.domain.entity.User;
import pl.bb166.inzchat.auth.domain.mapper.GroupMapper;
import pl.bb166.inzchat.auth.repository.GroupRepository;
import pl.bb166.inzchat.auth.repository.UserRepository;
import pl.bb166.inzchat.auth.util.Tuple;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class GroupServiceImpl implements GroupService {

    private UserRepository userRepository;

    private GroupRepository groupRepository;

    private GroupMapper groupMapper;

    @Autowired
    public void setGroupMapper(GroupMapper groupMapper) {
        this.groupMapper = groupMapper;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setGroupRepository(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    @Override
    public List<GroupDTO> gerAllRoomsForUser(String username) {
        return userRepository.getUserByUsername(username)
                .map(User::getGroups)
                .map(rooms -> rooms.stream()
                        .map(groupMapper::roomToRoomDTO)
                )
                .orElse(Stream.empty())
                .collect(Collectors.toList());
    }

    @Override
    public boolean addRoomToUser(String roomName, String username) {
        return groupRepository.findByName(roomName)
                .flatMap(rm ->
                        userRepository
                                .getUserByUsername(username)
                                .map(user -> Tuple.tuple(user, rm))
                ).map(element -> {
                            element._1.getGroups().add(element._2);
                            return element._1;
                        }
                ).map(userRepository::save)
                .isPresent();
    }

    @Override
    public boolean createRoom(String roomName) {
        if (!groupRepository.findByName(roomName).isPresent())
            return groupRepository
                    .save(
                            Group.builder()
                                    .name(roomName)
                                    .build()
                    ).getId() != null;
        else
            return false;
    }

    @Override
    public List<GroupDTO> searchByQuery(String query, String username) {
        return groupRepository
                .findRoomsByNameContaining(query, username)
                .stream()
                .map(groupMapper::roomToRoomDTO)
                .collect(Collectors.toList());
    }

    @Override
    public void removeUserFromGroup(String roomName, String username) {
        userRepository.getUserByUsername(username).map(user -> {
            user.setGroups(user.getGroups().stream()
                    .filter(element -> !element.getName().equals(roomName))
                    .collect(Collectors.toSet()));
            return user;
        }).ifPresent(userRepository::save);
    }
}
