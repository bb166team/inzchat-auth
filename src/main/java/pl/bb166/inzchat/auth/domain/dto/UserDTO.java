package pl.bb166.inzchat.auth.domain.dto;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    @Getter @Setter
    private String username;
}
