package pl.bb166.inzchat.auth.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Configuration
@EnableAsync
public class MainConfiguration {
    static Logger logger = LoggerFactory.getLogger(MainConfiguration.class);

    @Bean
    public MessageDigest sha265AlgorithmMessageDigest() throws NoSuchAlgorithmException {
        return MessageDigest.getInstance("SHA-256");
    }
}