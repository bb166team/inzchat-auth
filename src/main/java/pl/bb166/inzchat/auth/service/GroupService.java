package pl.bb166.inzchat.auth.service;

import pl.bb166.inzchat.auth.domain.dto.GroupDTO;

import java.util.List;

public interface GroupService {
    List<GroupDTO> gerAllRoomsForUser(String username);
    boolean addRoomToUser(String roomName, String username);
    boolean createRoom(String roomName);
    List<GroupDTO> searchByQuery(String query, String username);
    void removeUserFromGroup(String roomName, String username);
}