package pl.bb166.inzchat.auth.domain.dto;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RegisterUserDTO {
    @Getter
    private String username;
    @Getter
    private String password;
    @Getter
    private String name;
    @Getter
    private String surname;
    @Getter
    private String email;
}