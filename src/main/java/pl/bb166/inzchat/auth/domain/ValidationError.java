package pl.bb166.inzchat.auth.domain;

public enum ValidationError {
    USER_AVAILABLE,
    EMAIL_STRUCTURE,
    EMAIL_AVAILABLE,
    USERNAME_STRUCTURE,
    PASSWORD_STRUCTURE
}
