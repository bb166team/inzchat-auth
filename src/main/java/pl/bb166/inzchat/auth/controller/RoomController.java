package pl.bb166.inzchat.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.bb166.inzchat.auth.domain.dto.GroupDTO;
import pl.bb166.inzchat.auth.service.GroupService;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("room")
public class RoomController {

    private GroupService groupService;

    @Autowired
    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping
    public List<GroupDTO> rooms(Principal principal) {
        return groupService.gerAllRoomsForUser(
                principal.getName()
        );
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PutMapping("{roomName}")
    public ResponseEntity<Void> addRoomToUser(@PathVariable String roomName,
                                              Principal principal) {
        return groupService.addRoomToUser(roomName, principal.getName()) ?
                ResponseEntity.status(HttpStatus.CREATED).build() :
                ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();

    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PutMapping("{roomName}/{username}")
    public ResponseEntity<Void> addAnotherUserToRoom(@PathVariable String roomName, @PathVariable String username) {
        return groupService.addRoomToUser(roomName, username) ?
                ResponseEntity.status(HttpStatus.CREATED).build() :
                ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("{roomName}")
    public ResponseEntity<Void> addRoom(@PathVariable String roomName) {
        return groupService.createRoom(roomName) ?
                ResponseEntity.status(HttpStatus.CREATED).build() :
                ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();

    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("{query}")
    public List<GroupDTO> search(@PathVariable String query, Principal principal) {
        return groupService.searchByQuery(query, principal.getName());
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @DeleteMapping("{roomName}")
    public void delete(@PathVariable String roomName, Principal principal) {
        groupService.removeUserFromGroup(roomName, principal.getName());
    }
}