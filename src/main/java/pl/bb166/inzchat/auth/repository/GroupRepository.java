package pl.bb166.inzchat.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.bb166.inzchat.auth.domain.entity.Group;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface GroupRepository extends JpaRepository<Group, Long> {
    Optional<Group> findByName(String roomName);

    @Query(value = "select r.* from group_ r left join user_group ur on r.id = ur.group_id " +
                    "left join user_ u on u.id = ur.user_id " +
                    "where (r.name like %:queryl% and u.username is null) or (r.name like %:queryl% and u.username is not null and u.username != :username)",
            nativeQuery = true)
    List<Group> findRoomsByNameContaining(String queryl, String username);
}