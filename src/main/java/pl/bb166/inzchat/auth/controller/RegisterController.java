package pl.bb166.inzchat.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.bb166.inzchat.auth.domain.dto.RegisterUserDTO;
import pl.bb166.inzchat.auth.domain.dto.RegisterUserValidationDTO;
import pl.bb166.inzchat.auth.service.RegisterService;

@RestController
@RequestMapping("register")
public class RegisterController {
    private RegisterService registerService;

    @Autowired
    public void setRegisterService(RegisterService registerService) {
        this.registerService = registerService;
    }

    @PostMapping
    public ResponseEntity<RegisterUserValidationDTO> registerNewUser(@RequestBody RegisterUserDTO registerUserDTOBody) {
        return ResponseEntity.of(
                registerService.registerNewUser(registerUserDTOBody)
        );
    }
}
