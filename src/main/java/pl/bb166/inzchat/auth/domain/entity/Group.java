package pl.bb166.inzchat.auth.domain.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "group_")
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class Group {
    @Id
    @Getter @Setter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter @Setter
    private String name;
}
