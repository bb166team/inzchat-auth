package pl.bb166.inzchat.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bb166.inzchat.auth.domain.dto.PrivateSortDTO;
import pl.bb166.inzchat.auth.domain.dto.UserDTO;
import pl.bb166.inzchat.auth.domain.mapper.UserMapper;
import pl.bb166.inzchat.auth.repository.UserRepository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    private UserMapper userMapper;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public boolean addUserToUser(String username, String usernameToAdd) {
        PrivateSortDTO privateSortDTO = new PrivateSortDTO(username, usernameToAdd);
        return userRepository.getUserByUsername(privateSortDTO.getFirstUser()).map(user1 -> {
                    userRepository.getUserByUsername(privateSortDTO.getSecondUser()).ifPresent(user2 -> user1.getUsers().add(user2));
                    return user1;
        }).map(userRepository::save).isPresent();
    }

    @Override
    public boolean removeUserFormUser(String username, String userToRemove) {
        return userRepository.getUserByUsername(username)
                .map(user -> {
                    user.setUsers(
                        user.getUsers()
                                .stream()
                                .filter(user1 -> !user1.getUsername().equals(userToRemove))
                                .collect(Collectors.toSet())
                    );
                    return user;
                }).map(userRepository::save)
                .isPresent();
    }

    @Override
    public List<UserDTO> getAllUserForUser(String username) {
        return userRepository
                .getUserByUsername(username)
                .map(user ->
                        Stream.concat(user.getUsers().stream(), user.getUsersInverse().stream())
                                .map(userMapper::userToUserDTO)
                ).orElse(Stream.empty())
                .collect(Collectors.toList());
    }

    @Override
    public List<UserDTO> search(String query, String username) {
        return userRepository.searchByUsername(query, username)
                .stream()
                .map(userMapper::userToUserDTO)
                .collect(Collectors.toList());
    }
}