package pl.bb166.inzchat.auth.domain.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    @Getter @Setter
    private String username;

    @Getter @Setter
    private String password;

    @Getter @Setter
    private String email;

    @Getter @Setter
    private Boolean enabled;

    @Getter @Setter
    private String activationHash;

    @Getter @Setter
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.PERSIST)
    private Set<Role> roles;

    @Getter @Setter
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user_user",
            joinColumns = {@JoinColumn(name = "first_id")},
            inverseJoinColumns = {@JoinColumn(name = "second_id")}
    )
    private Set<User> users;

    @Getter @Setter
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user_user",
            joinColumns = {@JoinColumn(name = "second_id")},
            inverseJoinColumns = {@JoinColumn(name = "first_id")}
    )
    private Set<User> usersInverse;

    @Getter @Setter
    @ManyToMany
    @JoinTable(
            name = "user_group",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "group_id")}
    )
    private Set<Group> groups;
}
