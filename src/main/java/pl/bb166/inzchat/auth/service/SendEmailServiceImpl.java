package pl.bb166.inzchat.auth.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import java.text.MessageFormat;
import java.util.Properties;

@Service
public class SendEmailServiceImpl implements SendEmailService {
    private static final Logger logger = LoggerFactory.getLogger(SendEmailServiceImpl.class);

    private Properties properties = new Properties();

    private MessageFormat htmlContentTemplate;

   // @Value("${inzChat.domainUrl}")
    private String domainUrl;

   // @Value("${inzChat.mail.fromEmail}")
    private String fromEmail;

   // @Value("${inzChat.mail.fromName}")
    private String fromName;

  //  @Value("${inzChat.mail.subject}")
    private String subject;

  //  @Value("${inzChat.mail.smtp.address}")
    public void setSmtpAddress(String address) {
        properties.put("mail.smtp.host", address);
    }

  //  @Value("${inzChat.mail.smtp.username}")
    public void setSmtpUsername(String username) {
        properties.put("mail.smtp.user", username);
    }

  //  @Value("${inzChat.mail.smtp.password}")
    public void setSmtpPassword(String password) {
        properties.put("mail.smtp.password", password);
    }

    public SendEmailServiceImpl() {
        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.auth", "true");
    }

    //@Value("${inzChat.mail.smtp.port}")
    public void setPort(int port) {
        properties.put("mail.smtp.port", port);
    }

    //@Value("${inzChat.mail.contentTemplatePath}")
    public void loadHtmlTemplate(String path) throws IOException {
        htmlContentTemplate = new MessageFormat(new BufferedReader(
                new InputStreamReader(new ClassPathResource(path).getInputStream(), StandardCharsets.UTF_8)
        ).lines().reduce((a, e) -> a + e).orElseThrow(() -> new IOException("Read template error")));
    }

    @Async
    @Override
    public void sendActivationMessage(String email, String sha) {
        try {
            MimeMessage mimeMessage = new MimeMessage(Session.getInstance(properties, new Authenticator() {
                @Override
                public PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(
                            properties.getProperty("mail.smtp.user"),
                            properties.getProperty("mail.smtp.password")
                    );
                }
            }));

            Object[] parameters = new Object[] {domainUrl, sha};

            mimeMessage.setFrom(new InternetAddress(fromEmail, fromName));
            mimeMessage.setSubject(subject);
            mimeMessage.setContent(htmlContentTemplate.format(parameters), "text/html");
            mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(email));

            Transport.send(mimeMessage);
        } catch (Exception ex) {
           logger.error(ex.getMessage());
        }
    }
}