package pl.bb166.inzchat.auth.service;

import pl.bb166.inzchat.auth.domain.dto.UserDTO;

import java.util.List;

public interface UserService {
    boolean addUserToUser(String username, String usernameToAdd);
    boolean removeUserFormUser(String username, String userToRemove);
    List<UserDTO> getAllUserForUser(String username);
    List<UserDTO> search(String query, String username);
}
