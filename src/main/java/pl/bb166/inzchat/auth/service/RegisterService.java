package pl.bb166.inzchat.auth.service;

import pl.bb166.inzchat.auth.domain.dto.RegisterUserDTO;
import pl.bb166.inzchat.auth.domain.dto.RegisterUserValidationDTO;

import java.util.Optional;

public interface RegisterService {
    Optional<RegisterUserValidationDTO> registerNewUser(RegisterUserDTO registerUserDTO);
    void activateUser(String sha);
}