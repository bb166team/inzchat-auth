package pl.bb166.inzchat.auth.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import pl.bb166.inzchat.auth.domain.dto.RegisterUserDTO;
import pl.bb166.inzchat.auth.domain.dto.UserDTO;
import pl.bb166.inzchat.auth.domain.entity.User;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class UserMapper {

    private PasswordEncoder passwordEncoder;

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Mapping(source = "register.password", qualifiedByName = "bcryptEncode", target = "password")
    public abstract User registerUserDTOToUser(RegisterUserDTO register);

    @Named("bcryptEncode")
    public String bcryptEncode(String password) {
        return passwordEncoder.encode(password);
    }

    public abstract UserDTO userToUserDTO(User user);
}