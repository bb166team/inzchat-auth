package pl.bb166.inzchat.auth.service;

public interface SendEmailService {
    void sendActivationMessage(String email, String sha);
}
